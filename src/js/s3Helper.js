const AWS = require('aws-sdk')
const cdn_config = require('../../cdn.config.json')

const s3 = new AWS.S3({
  accessKeyId: process.env.s3AccessKeyId,
  secretAccessKey: process.env.s3SecretAccessKey,
  bucket: cdn_config.bucket_name,
  region: cdn_config.region,
  cache: false // Evitar que por cache local no se suban los archivos
})

let listObjects = (prefix) => {
  let objects
  s3.listObjectsV2({
    Bucket: cdn_config.bucket_name,
    Prefix: prefix,
  }).on('success', (response) => {
    objects = response.data.Contents
    console.log('success', objects)
  }).send()
  return objects
}

export {
  listObjects
}